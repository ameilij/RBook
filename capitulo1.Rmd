---
title: 'Capitulo 1: Operadores Matemáticos'
author: "Ariel Meilij"
date: "October 29, 2016"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Operadores Matemáticos

La consola de R puede parecer un lugar extraño si alguién es nuevo a la programación, y no tanto si el lector viene de Python o Ruby. Sin embargo es increible la cantidad de trabajo que uno puede lograr en la consola de R cuando el trabajo es pesado en el sentido matemático. Empecemos con algo muy sencillo, sumar dos más dos. Todos los libros de programación empiezan con un programa sencillo que imprime en la pantalla "Hello World!". Pero nosotros somos a) matemáticos en el corazón y b) Latinos, no necesitamos hablar inglés. Así que empecemos con algo muy básico. 

```{r}
2 + 2
```

El lector verá varias cosas que solo tenemos que explicar una vez y nunca más. 

* Todo el código fuente, lo que hay que tipear en la consola, sale en fondo gris.

* El output de R es visible porque va a salir con dos _hashtags_ estilo ## y luego casi siempre corchetes con un número 1 adentro. El número puede cambiar. Básicamente R nos dice que la respuesta es un objeto cuyo primer componente es la salida a pantalla. 

Lo importante es R nos dió la respuesta correcta, un cuatro. Probemos operaciones un poco más complicadas como suma y restas, multiplicaciones, y raices cuadradas.

```{r}
2 + 3 * 4 - 5 / 3

sqrt(2)

10%%3

10%/%3
```

Si el lector es astuto (lo es, compraron este libro) verá como funciona R en cuanto a las reglas de operandos.

* En el primer ejemplo, los operadores con mayor prioridad - los que se usan para saber donde romper los términos y que hacer primero - son la suma y la resta. Por eso primero R suma dos a la multiplicación de tres por cuatro, y luego a eso le resta la división de cinco dividido tres. La regla correcta es ser prolijo y usar parentesis cuando las operaciones se compliquen. 

* El segundo ejemplo es la raíz cuadrada de 2. En este caso usamos la función <code>sqrt<code> y pasamos entre paréntesis el número del cual queríamos la raíz cuadrada, en este caso el dos. A pesar que somos Latinos, a R lo escribió  Ross Ihaka y Robert Gentleman de la Universidad de Auckland, donde hablán inglés, y por eso debemos aprender aunque sea el poquito de qué es cada cosa en la matemática. 

* El tercer ejemplo es el módulo de división. El módulo de división es el remanente entero que queda después de dividir un número por otro. Cuando dividimos diez entre tres, y cómo diez no es divisible por tres, nos da de resultado un tres y un restante de uno, que no se puede dividir entre tres en números enteros. Por eso R nos devuelve como respuesta un uno.

* El cuarto ejemplo es la división por enteros. Diez dividido tres da tres, que es el cociente, y un restante de 1, que ya vimos es el modulo de arriba.

No voy a aburrir a nadie con ejemplos básicos ya que tuve un profesor en la universidad así y lo odiaba realmente. Prefiero pasar a ejemplos mucho más entretenidos de las cosas que no son tan sencillas o prevesibles en R. Veamos un caso sencillo y que nos volveremos a encontrar de aquí en adelante.

```{r}
a = 1
a
```

En programación decimos que a es una variable. Es variable porque el valor que toma puede variar. En el ejemplo de arriba a la variable __a__ le dimos el valor de uno. Piensen que una variable es un cajón y que adentro del cajón guardamos cosas para buscarlas después. Le asignamos a la variable __a__ el valor uno utilizando el operador de asignación signo igual.

__Pero nadie usa el signo igual en R...__

De ahora en adelante vamos a usar el operador de asignación oficial que es <-. Todos los muchachos y muchachas populares lo usan, además de toda la gente de edad avanzada que alguna vez lo uso en C (no C++ o C#, sino C a la antigua). Es una cuestión de costumbre. Para los que usan RStudio para escribir sus programas es bueno que sepan que si hacen Alt y - a la vez sale de forma automática. 

No solo podemos asignar números, una variable puede ser muchas cosas.

```{r}
miVariable <- "Tremenda Variable la Mia..."
miOtraVariable <- 3.14159

miVariable
miOtraVariable
```

Podemos guardar texto usando un __string__. Un __string__ es una cadena de caracteres literales y esto es constante en todos los lenguajes de programación modernos. Recuerden utilizar las comillas y cerrarlas para no tener error en sus programas. En R también podemos hacer lo siguiente:

```{r}
numeros <- 10:15
numeros
```

En el último ejemplo R acumuló los valores del diez al quince en la variable <code>numeros</code>. Aquí las cosas se ponen entretenidas. Si uno le asigna el valor 5 a la variable b, y luego le suma 1, no hay que ser un científico para adivinar que da seis. 

```{r}
b <- 5
b + 1
```

Pero si sumamos 1 a la variable numeros, ¿qué dará? Es una cadena de enteros del diez al quince. ¿A quién le suman el 1? ¿Al 10, al 15? ¿A todos?

```{r}
numeros <- 10:15
numeros + 1
```

En el ejemplo de arriba vemos que R tiene una forma de pensar muy particular. R asume lo siguiente: _"Si tengo una cadena de números del diez al quince y me dicen que tengo que evaluar la cadena más uno, asumo que debo sumar uno a cada elemento de la cadena y retornar una lista de las sumas sin cambiar o modificar la cadena original"._

__Primera Lección de R:__ Hay dos formas de hacer las cosas en R, una mal como uno piensa, y una correcta que es como R piensa. Es ciencia, no democracia. 

## Evaluando Operaciones
R tiene un hábito increible de evaluar operaciones y darnos el resultado correcto (pero no necesariamente lo que esperabamos).

```{r}
masNumeros <- 100:120
masNumeros < 115
```

En el ejemplo arriba la variable _masNumeros_ toma los valores del cien al ciento veinte. Pero cuando le pedimos a R evaluar _masNumeros_ menor que ciento quince nos devuelve una cadena de veinte valores lógicos. Los valores toman solo dos posibles resultados: __FALSE__ para cuando la evaluación es falsa (ese valor en particular no es menor que ciento quince) y __TRUE__ cuando la evaluación es real (ese valor en particular es menor que ciento quince). Cuando R tuvo que evaluar si 115 < 115 dió como resultado falso, ya que ciento quince no es menor que ciento quince, es igual, pero no menor.

Este ejemplo es importante porque influye en nuestra forma de ver y programar un problema en R. Supongamos que tenemos en la variable masNumeros veinte valores y el ejercicio es imprimir los valores que son menores que ciento quince. La solución no es tan sencilla como tipear ```masNumeros < 115``` porque ya sabemos que R nos va a devolver una cadena de falsos y verdaderos que aunque correctos, no nos ayudan de nada. La solución correcta es la de abajo:

```{r}
masNumeros[masNumeros < 115]
```

Sin embargo si pedimos el inverso:

```{r}
masNumeros[masNumeros > 115]
```

Si ven las dos listas ya se habrán dado cuenta que R nos robó el 115 de la lista. R tiene muy claro si pedimos evaluar la cadena entera que lo tiene que listar como FALSE. Pero al imprimir parece no darse cuenta o no saber que poner y no lo imprime ni en una ni en otra. R no es un lenguaje de ambiguedades sino que responde muy exactamente. La solución es utilizar un indice de búsqueda más exacto como ```masNumeros => 115``` cuando programamos. Pero lo importante es saber que la sintáxis de R a veces nos obliga a un pensamiento muy estricto matemático para no caer en errores. 





